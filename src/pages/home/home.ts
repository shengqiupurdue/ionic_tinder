import { Component, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { ChatmainPage } from '../chatmain/chatmain';
import { ChatindPage } from '../chatind/chatind';
import { PhotoPage } from '../photo/photo';
import { LoginPage } from '../login/login';
import { DomSanitizer } from "@angular/platform-browser";
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  ready = false;
  private places: any;
  attendants = [];
  cardDirection = "xy";
  cardOverlay: any = {
    like: {
      backgroundColor: '#28e93b'
    },
    dislike: {
      backgroundColor: '#e92828'
    }
  };

  constructor(
    public navCtrl: NavController,
    private sanitizer: DomSanitizer,
    public http: HttpClient
  ) {

    this.http.get('http://localhost:8080/test.json')
      .subscribe(data => {
        this.places = data;
        console.log(data);
        for (let i = 0; i < this.places.length; i++) {
          this.attendants.push({
            id: i + 1,
            likeEvent: new EventEmitter(),
            destroyEvent: new EventEmitter(),
            asBg: sanitizer.bypassSecurityTrustStyle('url(' + this.places[i].path + ')'),
            text: this.places[i].text
          });
        }
      });

    this.ready = false;
  }

  onCardInteract(event) {
    console.log(event);
  }


  prof(){
    this.navCtrl.push(ProfilePage);
  }
  chatm(){
    this.navCtrl.push(ChatmainPage)
  }
  chatid(){
    this.navCtrl.push(ChatindPage)
  }
  pho(){
    this.navCtrl.push(PhotoPage)
  }
  lgn(){
    this.navCtrl.push(LoginPage)
  }
}
